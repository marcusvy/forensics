![alt text](https://ipog.edu.br/wp-content/uploads/2020/08/Logo.png "IPOG")
## MBA Informática Forense
### Análise Forense do Sistema Operacional UNIX**

**Docente: Renam Ferreira Cavalheiro** <renan.f.cavalheiro@gmail.com>

**Discente: Marcus Vinícius da Rocha Gouveia Cardoso** <marcusvy@gmail.com>

# Utilizando o volatility no Linux para dump de memória

O Volatility Framework é uma coleção completamente aberta de ferramentas,
implementado em Python sob a GNU General Public License, para o
extração de artefatos digitais de amostras de memória volátil (RAM).
As técnicas de extração são realizadas de forma completamente independente do
sistema sendo investigado, mas oferece visibilidade no estado de tempo de execução
do sistema. A estrutura tem como objetivo apresentar às pessoas o
técnicas e complexidades associadas à extração de artefatos digitais
a partir de amostras de memória volátil e fornecer uma plataforma para trabalhos futuros.

## Preparando o ambiente

Para o ambiente estamos utiliizado a distro SIFT Workstation da Sans Forensics, que é baseada no Ubuntu 18.04.4 LTS. Pode ser obtido no link <https://digital-forensics.sans.org/community/downloads>.

Criemos um diretório para noso trabalho. Decidi colocar o nome de "Projeto".

```bash
cd /home/sansforensics/
mkdir Projeto
cd Projeto
```

Copiei um arquivo de exemplo **dump.dmp** para o diretório "Projeto".

## Requisitos (Dependências)

O pacote **dwarfdump** é utilizado pelo volatility para imprimir ou verificar as seções DWARF conforme necessário. Optei por instalar utilizando o gerenciador de pacotes nativo do GNU/Linux Debian, o **Apt** ( Ferramenta de Empacotamento Avançada) através do comando:

```bash
apt install dwarfdump
```

Instalado com sucesso, agora basta obter o programa Volatility. Podemos obter o código fonte no repositório de código Github. O código será obtido através da clonagem do repositório na nossa máquina.

```bash
git clone https://github.com/volatilityfoundation/volatility.git
```

Será criado um diretório com nome **volatility**.

```bash
root@siftworkstation:/home/sansforensics/# ls
dump.dmp  volatility
```

## Gerar um módulo DWARF

Tendo o código fonte em nossa máquina precisamos fazer o volatility rodar em nosso sistema linux. Para isso iremos gerar um Profile (perfil) compatível com a versão do nosso kernel linux. Acessemos o diretório **./volatility/tools/linux/** e compilamos (ou melhor geramos) um módulo DWARF.

```bash
cd volatility/tools/linux/
make
```

Irá gerar um arquivo **module.dwarf**. Este arquivo é muito importante! Logo iremos utilizá-lo.

## Criar um perfil para o volatility

Tendo nosso **module.dwarf**. Vamos criar o nosso _Profile_ para o volatility. Por motivos de organização, vamos obter as informações da versão do kernel linux que estamos utilizando rodando o comando:

```bash
root@siftworkstation:/home/sansforensics/Project/volatility/tools/linux#
uname -a

Linux siftworkstation 4.18.0-15-generic #16~18.04.1-Ubuntu SMP Thu Feb 7 14:06:04 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
```

No nosso caso estamos na máquina **siftworkstation** utilizando a versão do kernel **4.18.0**. Para isso precisamos de dois arquivos:

- **module.dwarf**: Que obtemos na etapa anterior;
- **System.map-(version)**: No sistema operacional Linux, o arquivo System.map é uma tabela de símbolos do núcleo do sistema. Através desta tabela é possível consultar os endereços de cada símbolo na memória, o que pode ser necessário quando se realiza a depuração. Pode ser encontrado no diretório /boot/. No nosso caso utilizaremos o arquivo: _/boot/System.map-4.18.0-15-generic_.

Criemos o perfil compactando os arquivos acima citados através do compando zip. Para ficar implícito, optemos gerar um perfil de nome: **siftworkstation_4.18.0**. Que é o nome da máquina mais a versão atual do kernel linux.

```bash
zip siftworkstation_4.18.0 module.dwarf /boot/System.map-4.18.0-15-generic
```

Obtemos assim o arquivo **siftworkstation_4.18.0.zip**, que é o perfil do volatility.

# Instalar o Profile gerado

Copie **siftworkstation_4.18.0.zip** para o diretório **<diretorio_de_instalação>/volatility/plugins/overlays/linux/** e terá instalado nosso perfil.

```bash
root@siftworkstation:/home/sansforensics/Project/volatility/tools/linux#

cp siftworkstation_4.18.0 ../../volatility/plugins/overlays/linux/
```

Em seguida podemos checar se realmente foi instalado.

```bash
root@siftworkstation:/home/sansforensics/Project/volatility#
python vol.py --info

Volatility Foundation Volatility Framework 2.6.1


Profiles
--------
Linuxsiftworkstation_4_18x64 - A Profile for Linux siftworkstation_4.18 x64
VistaSP0x64                  - A Profile for Windows Vista SP0 x64
VistaSP0x86                  - A Profile for Windows Vista SP0 x86
VistaSP1x64                  - A Profile for Windows Vista SP1 x64
VistaSP1x86                  - A Profile for Windows Vista SP1 x86

...

```

Na seção "Profiles" podemos ver um perfil para Linux siftworkstation_4.18 x64, que trata-se do perfil que geramos adicionado do tipo de arquitetura que foi gerado. A esquerda temos o nome do Profile **Linuxsiftworkstation_4_18x64**, indicando assim que foi instalado com sucesso.

## Executando o Volatility

Voltemos para a nossa pasta do projeto.

```bash
cd /home/sansforensics/Project/
```

O volatility foi escrito em linguagem Python, e utiliza o arquivo principal **vol.py**.
Para executar o volatility, basta dar o comando:

```bash
python volatility/vol.py
```

Agora para depurar execute o volatility referenciando o perfil através do argumento "--profile", o arquivo de dump de memória através do argumento "-f" e em seguida o plugin do volatility. Por exemplo, suponha que queira rodar o plugin **linux_bash**, através do profile **Linuxsiftworkstation_4_18x64**, para depurar o arquido de dump de memória **dump.dmp**, o comando será:

```bash
python volatility/vol.py --profile=Linuxsiftworkstation_4_18x64 -f dump.dmp linux_bash
```


## Referências

* [Python.org](http://www.python.org)
* [Volatility Foundation](https://www.volatilityfoundation.org/about)
* [Volatility Github](https://github.com/volatilityfoundation/volatility)
* [Volatility Usage](https://github.com/volatilityfoundation/volatility/wiki/Volatility-Usage)
* [Volatility Linux](https://github.com/volatilityfoundation/volatility/wiki/Linux)
